﻿# LINQ

Lenguaje integrado de consultas del ingles (Language Integrated Query).
Añade capacidades de consulta de datos a .Net. con *expresiones de consulta* similares a SQL las cuales pueden usarse para extraer y procesar datos de arreglos, clases enumerable y documentos XML, y tambien de Base de datos.

Para poder usar Linq en nuestro programas debemos importar el espacio de nombres ***System.Linq***

    using System.Linq;


## Expresiones de Consulta

Las expresiones de consulta deben empezar con *from* y terminar con *select* o *group*


``` csharp
//origen de datos
int[] calificaciones = {16, 17, 11, 14, 18, 19};
// expresion de consulta
IEnumerable<int> consultaCalificaciones = 
        from calificacion in calificaciones     // clausula requerida
        where score>14                          // clausula opcional
        order by score descending               // clausula opcional
        select score;                           // clausula requerida 

foreach(int calificacion in consultaCalificaciones)
    Console.WriteLine($" calificacion : {calificacion}");
```

las consultas tambien pueden ejecutarse sobre listas tipadas

``` csharp
class Estudiante {
    public string Nombre {get; set;}
    public int Calificacion {get; set;}
}

List<Estudiante> estudiantes = TraerEstudiantes();

IEnumerable<Estudiante> estudiantesAprobados = 
                from estudiante in estudiantes
                where estudiante.Calificacion >= 14
                select estudiante;

foreach(Estudiante estudiante in estudiantes)
    Console.WriteLine($"{estudiante.Nombre} aprobado con {estudiante.Calificacion});
```

## Ejecución Diferida
Las consultas *Linq* no se ejecutan en l momento que se declaran o definen, si no en el momento que se acceden.

![Ejecución Diferida](images/linq_ejecucion.png)

para verificarlo puedes copiar con esto:

``` csharp
class EjecucionDiferida
{
    public static void Main(string[] args)
    {
        List<int> enteros = new List<int> { 34, 45, 67, 1, 4, 6, 8, 12, };
        IEnumerable<int> consulta = from num in enteros
                                        where num > 10
                                        orderby num
                                        select num;

        foreach (int numero in consulta)
        {
            Console.WriteLine($"{numero}");
        }
        enteros.Add(80);
        Console.WriteLine("El arreglo fue modificado");
        foreach (int numero in consulta)
        {
            Console.WriteLine($"El numero es: {numero}");
        }
    }
}
```

Es posible *forzar* la ejecución de las consultas utilizando los metodos de extension: *ToList()* o *ToArray()*

``` csharp
    List<int> enteros = new List<int> { 34, 45, 67, 1, 4, 6, 8, 12, };
    List<int> consulta = (from num in enteros
                                    where num > 10
                                    orderby num
                                    select num).ToList();

```

## Metodos de Extension
*Linq* ofrece métodos de extensión que permiten realizar las mismas consultas sobre las colecciones de objetos usando expresiones lambda.

Por ejemplo:

``` csharp
    IEnumerable<int> enteros = new List<int> { 34, 45, 67, 1, 4, 6, 8, 12, };
    List<int> resultado = enteros
                            .Where(num=> num>10)
                            .Orderby(num=>num)
                            .ToList();
                            .ForEach(num => Console.WriteLine(num));

```

``` csharp
class Estudiante {
    public string Nombre {get; set;}
    public int Calificacion {get; set;}
    public string Periodo {get; set;}
}

// TraeEstudiantes es un metodo que devuelve un IEnumerable<Estudiante>

TraeEstudiantes()
        .Where(alumno => alumno.Calificacion>=14)
        .OrderByDescending(alumno => alumno.Calificacion)
        .Take(4)
        .Select(alumno => new {alumno.Nombre})
        .ToList()
        .ForEach(alumno => Console.Writeline(alumno.Nombre));
    
```




## Operaciones Básicas

### Filtrado

``` csharp
// linq
IEnumerable<Cliente> clientesDeLima = from cliente in clientes
                                        where cliente.Ciudad == "Lima"
                                        select cliente;
//metodos de extension
IEnumerable<Cliente> clientesDeLima  = clientes.Where(c => c.Ciudad == "Lima");
```

### Ordernamiento

``` csharp
IEnumerable<Cliente> clientesDeLima = from cliente in clientes
                                        where cliente.Ciudad == "Lima"
                                        order by cliente.Apellido
                                        select cliente;

IEnumerable<Cliente> clientesDeLima  = clientes
                                        .Where(c=>c.Ciudad=="Lima")
                                        .OrderBy(c=>c.Apellido);
```

### Agrupamiento

``` csharp
IEnumerable<IGrouping<string,Cliente>> clientesPorCiudad = from cliente in clientes
                                        group cliente by cliente.Ciudad;

foreach(var grupo in clientesPorCiudad)
{
    Console.WriteLine($"Grupo: {grupo.Key}");
    foreach(Cliente cliente in grupo)
        Console.WriteLine(cliente.Nombre);
}

// usando metodos de extension

clientes.GroupBy(c=>c.Ciudad)
        .ToList()
        .ForEach(grupo => {
            Console.WriteLine(grupo.Key);
            g.Tolist()
              .ForEach(cliente => Console.WriteLine(cliente.Nombre)); 
        });
```

#### Implementando Having:
 

 ``` csharp
IEnumerable<IGrouping<string,Cliente>> clientesPorCiudad = from cliente in clientes
                                        group cliente by cliente.Ciudad into grupo
                                        where grupo.Count()>1
                                        select grupo;

foreach(var grupo in clientesPorCiudad)
{
    Console.WriteLine($"Grupo: {grupo.Key}");
    foreach(Cliente cliente in grupo)
        Console.WriteLine(cliente.Nombre);
}

// con metodos de extensión

clientes.GroupBy(c=>c.Ciudad)
        .Where(grupo => grupo.Count()>1)
        .ToList()
        .ForEach(grupo => {
            Console.WriteLine(grupo.Key);
            g.Tolist()
              .ForEach(cliente => Console.WriteLine(cliente.Nombre)); 
        });
```

### Combinación

``` csharp
var consulta = form cliente in clientes
               join vendedor in Vendedores
               on cliente.Ciudad equals vendedor.Zona
               select new { Vendedor = vendedor.Nombre, Cliente=cliente.Nombre, cliente.Ciudad };


// usando metodos de extension

var consulta = clientes.Join(Vendedores,
	      c => c.Ciudad,
	      v => v.Ciudad,
	      (c, v) => new { Vendedor = v.Nombre, Cliente = c.Nombre, c.Ciudad});
```