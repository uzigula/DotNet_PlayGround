﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroLinq
{
    class Program
    {
        static void Main(string[] args)
        {
            Linqbasico();
        }

        private static void EjemploYield()
        {
            var numeros = new List<int> {100, 125, 75, 11, 5, 78, 900, 34, 45, 67, 1, 4, 6, 8, 12,};

            Console.WriteLine("usando yield return");
            traer(numeros).Take(3).ToList().ForEach(num => Console.WriteLine($"{num}"));

            Console.WriteLine("take con un for");
            int i = 1;
            foreach (int numero in traer(numeros))
            {
                Console.WriteLine(numero);
                i++;
                if (i >= 4) break;
            }

            Console.WriteLine("sin yield return");
            traerLista(numeros).Take(3).ToList().ForEach(num => Console.WriteLine($"{num}"));
        }

        private static IEnumerable<int> traer(IEnumerable<int> lista)
        {
            foreach (int numero in lista)
            {
                if (numero > 10)
                {
                    Console.WriteLine($"Devolviendo {numero}");
                    yield return numero;
                }
            }
        }


        private static List<int> traerLista(IEnumerable<int> lista)
        {
            List<int> resultado = new List<int>();
            foreach (int numero in lista)
            {
                if (numero > 10)
                {
                    Console.WriteLine($"Devolviendo {numero}");
                    resultado.Add(numero);
                }
            }
            return resultado;
        }

        private static void LinqFuncional()
        {
            List<int> enteros = new List<int> { 100, 125, 75, 11, 5, 78, 900, 34, 45, 67, 1, 4, 6, 8, 12, };
            Stopwatch reloj = new Stopwatch();
            reloj.Start();
            // linq estructurado
            foreach (int numero in enteros.Where(num => num > 10))
            {
                Console.WriteLine($"{numero}");
            }
            reloj.Stop();
            Console.WriteLine($"tiempo {reloj.ElapsedMilliseconds}");
            reloj.Reset();
            reloj.Start();
            //Linqbasico funcional al estilo fluente
            enteros
                .Where(num => num > 10)
                .OrderByDescending(num => num)
                .ToList()
                .ForEach(resultado => Console.WriteLine($"{resultado}"));
            reloj.Stop();
            Console.WriteLine($"tiempo {reloj.ElapsedMilliseconds}");
        }

        private static void Linqbasico()
        {
            List<int> enteros = new List<int> { 34, 45, 67, 1, 4, 6, 8, 12, };
            Console.WriteLine("basico forma1");
            Stopwatch reloj = new Stopwatch();
            reloj.Start();
            // basica estructurada
            for (int i = 0; i < enteros.Count; i++)
            {
                if (enteros[i] > 10) Console.WriteLine($"{enteros[i]}");
            }

            reloj.Stop();
            Console.WriteLine($"tiempo {reloj.ElapsedMilliseconds}");
            Console.WriteLine($"basico forma2 ");
            reloj.Reset();
            reloj.Start();
            // estructurada para colecciones de un tipo
            foreach (int numero in enteros)
            {
                if (numero > 10) Console.WriteLine($"{numero}");
            }
            reloj.Stop();
            Console.WriteLine($"tiempo {reloj.ElapsedMilliseconds}");

            // linq forma 1
            reloj.Reset();
            reloj.Start();

            Console.WriteLine("linq forma1");
            List<int> consulta = (from num in enteros
                                  where num > 10
                                  select num).ToList();

            IEnumerable<int> consulta2 = from num in enteros
                                         where num > 10
                                         orderby num
                                         select num;

            foreach (int numero in consulta)
            {
                Console.WriteLine($"{numero}");
            }

            reloj.Stop();
            Console.WriteLine($"tiempo {reloj.ElapsedMilliseconds}");

            enteros.Add(80);
            Console.WriteLine("El arreglo fue modificado");
            foreach (int numero in consulta2)
            {
                Console.WriteLine($"{numero}");
            }
        }
    }
}
