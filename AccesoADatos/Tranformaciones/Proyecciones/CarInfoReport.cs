﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tranformaciones.Proyecciones
{
    class CarInfoReport
    {
        public string VIN { get; set; }
        public string ConfigurationModelNumber { get; set; }
        public int ConfigurationModelYear { get; set; }
    }
}
