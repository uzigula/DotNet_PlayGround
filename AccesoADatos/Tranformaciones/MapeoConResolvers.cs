﻿using MVC5Course.Automapper.Domain.BasicDomainExample;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tranformaciones.Proyecciones;
using AutoMapper;

namespace Tranformaciones
{
    class MapeoConResolvers
    {
        static void Main(string[] args)
        {
            AutoMapper.Mapper.CreateMap<double, DateTime>().ConvertUsing<UnixDateConverter>();
            AutoMapper.Mapper.CreateMap<Employee, EmpleadoVistaEscalafon>()
                .ForMember(dest => dest.YearsInTheCompany,
                opt => opt.ResolveUsing<YearInCompanyResolver>())
                .ForMember(dest => dest.Edad,
                opt => opt.ResolveUsing<EdadEmpleadoResolver>());
                
                // ignorar el destino .ForMember(dest => dest.FechaContrato, opt => opt.Ignore());
                // ignorar el origen .ForSourceMember(orig => orig.FechaContrato, opt => opt.Ignore());




            var empleado = new Employee
            {
                Name = "Juan Perez",
                HireDate = new DateTime(2013, 11, 25),
                DateOfBirth = new DateTime(1981,11,25),
                FechaContrato = 186400
            };

            var empleadoVista = AutoMapper.Mapper.Map<Employee, EmpleadoVistaEscalafon>(empleado);

            Console.WriteLine(empleadoVista.YearsInTheCompany);
            Console.WriteLine(empleadoVista.Edad);
            Console.WriteLine(empleadoVista.FechaContrato);
        }
    }

    class YearInCompanyResolver : ValueResolver<Employee, int>
    {
        protected override int ResolveCore(Employee source)
        {
            return (int)((DateTime.Now - source.HireDate).TotalDays / 365);
        }
    }

    class EdadEmpleadoResolver : ValueResolver<Employee, int>
    {
        protected override int ResolveCore(Employee source)
        {
            return (int)((DateTime.Now - source.DateOfBirth).TotalDays / 365);
        }
    }


    class UnixDateConverter : ITypeConverter<double, DateTime>
    {
        public DateTime Convert(ResolutionContext context)
        {
            var origen = (double)context.SourceValue;
            var unixEpoch = new DateTime(1970, 1, 1).ToLocalTime();
            return unixEpoch.AddSeconds(origen).ToLocalTime();
        }
    }


}
