﻿using MVC5Course.Automapper.Domain.BasicDomainExample;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tranformaciones.Proyecciones;

namespace Tranformaciones
{
    class MapeoPorConvenciones
    {
        static void Main(string[] args)
        {
            // COnfiguracion de la Transformacion
            AutoMapper.Mapper.CreateMap<CarSetup, CarInfoReport>();

            var car = new CarSetup()
            {
                Configuration = new EngineConfiguration { ModelYear = 2015, ModelNumber = "XYZ254AF" },
                DefectRate = 5,
                ExportCode = new Guid(),
                Maker = "Ford",
                VIN="KAF877875"
            };
            
            // Trasnformando CarSetup en CarInfoReport
            CarInfoReport reporte = AutoMapper.Mapper.Map<CarSetup, CarInfoReport>(car);
            reporte.ConfigurationModelNumber = car.Configuration.ModelNumber;

            Console.WriteLine($"{reporte.VIN} {reporte.ConfigurationModelYear} {reporte.ConfigurationModelNumber}");

        }
    }
}
