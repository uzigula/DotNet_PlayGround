﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intro
{
    class Comandos
    {
        public static void LeerInformacion()
        {
            Console.WriteLine("Leyendo informacion usando un data reader");
            using (SqlConnection conexion = new SqlConnection())
            {
                conexion.ConnectionString = "Server=S222-VS-SERV;Database=Chinook;Trusted_Connection=True;";
                conexion.Open();
                Console.WriteLine($"Conectado a {conexion.Database}");
                using (SqlCommand consulta = new SqlCommand())
                {
                    consulta.CommandText = "Select Title, AlbumId from Album";
                    consulta.CommandType = CommandType.Text;
                    consulta.Connection = conexion;
                    IDataReader lector = consulta.ExecuteReader();

                    Console.WriteLine("Registros Encontrados");
                    Console.WriteLine("Titulo                     AlbumId");
                    while (lector.Read())
                    {
                        Console.WriteLine($"{lector["Title"]} {lector[1]}");
                    }
                    lector.Close();
                }

                conexion.Close();
            }

        }
        
        public static void ActualizarInformacion()
        {
            Console.WriteLine("Actualizando el Album 1");
            using (SqlConnection conexion = new SqlConnection())
            {
                try
                {
                    conexion.ConnectionString = "Server=S222-VS-SERV;Database=Chinook;Trusted_Connection=True;";
                    conexion.Open();
                    Console.WriteLine($"Conectado a {conexion.Database}");
                    using (SqlCommand consulta = new SqlCommand())
                    {
                        consulta.CommandText = "update Album set Title = Title + ' (modificado)' where Albumid=1";
                        consulta.CommandType = CommandType.Text;
                        consulta.Connection = conexion;
                        consulta.ExecuteNonQuery();
                    }

                }
                catch (Exception)
                {
                    // tratar el error
                    throw;
                }
                finally
                {
                    if (conexion.State == ConnectionState.Open) conexion.Close();
                }
            }

        }
    }
}
