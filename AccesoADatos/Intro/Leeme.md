﻿# Acceso a Datos

Llegado un momento una Aplicación debe guardar (persistir) información en algun repositorio de datos. Actualmente tenemos diferentes tipos Repositorio de Datos :

+ Base de Datos Relacionales 
+ NoSQL (aqui hay una gran variedad)
  + Basadas en Clave/Valor (Key value)
  + Basadas en Documentos (Document Based)
  + Basadas en Columnas (Column Based)
  + Basadas en Grafos (Graph Based)

Por lo tanto debemos analizar dos aspectos a la hora escoger un repositorio de datos para nuestras aplicaciones

1. Naturaleza de la Información 
  + Analizar la estructura
  + Analizar como se accede,consulta
2. Naturaleza de la Aplicacion
  + Como la aplicación usa la Información (Presenta, Procesa)



## Repositorios Relacionales

Estos son las Bases de Datos convencionales, y existen multiples productos que podemos utilizar, particularmente para aplicaciones desarrolladas en .net podemos usar casi todas (al menos las que yo conozco).

+ [Sql Server]
+ [Oracle]
+ [MySQL]
+ [PostGreSQL]
+ [DB2]


## Ado .Net

Es el framework que nos permite acceder a las diferentes Bases de Datos.
Las interfaces que nos brinda el framework

+ IDbConnection
+ IDbCommand
+ IDbDataAdapter
+ IDbDataParameter
+ IDataReader
+ IDbTransaction


### Configuración de la Base de Datos

Abrir el archivo de la base datos Chinook ubicado en :

    AccesoADatos\BDScripts\Chinook_SqlServer_AutoIncrementPKs.Sql

Ejecutarlo desde SQL Server Management Studio


Para ver como son las cadenas de conexion puede ir a [ConnectionStrings.org].
Ahi hay un compendio de todas las cadenas de conexion para casi cualquier origen de datos.




[Sql Server]: <https://www.microsoft.com/en-us/cloud-platform/sql-server>
[Oracle]: <http://www.oracle.com/database>
[MySQL]: <http://www.mysql.com>
[PostGreSQL]: <http://www.postgresql.org>
[DB2]: <http://www.ibm.com/analytics/us/en/technology/db2/>
[ConnectionStrings.org]: <http://www.connectionstrings.com/>