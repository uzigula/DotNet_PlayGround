﻿using System;
using System.Data.SqlClient;

namespace Intro
{
    class Conexiones
    {
        public static void EjemploConexionRecomendado()
        {
            using (SqlConnection conexion = new SqlConnection())
            {
                conexion.ConnectionString = "Server=S222-VS-SERV;Database=Chinook;Trusted_Connection=True;";
                conexion.Open();
                Console.WriteLine($"Conectado a {conexion.Database}");
                conexion.Close();
            }
        }

        public static void EjemploConexion()
        {
            // mala practica
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Server=S222-VS-SERV;Database=Chinook;Trusted_Connection=True;";

            conexion.Open();

            Console.WriteLine($"Conectado a {conexion.Database}");
        }

    }
}
