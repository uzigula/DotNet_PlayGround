﻿using PatronRepositorio.Dominio;
using PatronRepositorio.Implementaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatronRepositorio
{
    public class LinqEjemplos
    {
        public static void Main(string[] args)
        {
            var cadenaConexion = "Server=S222-VS-SERV;Database=Chinook;Trusted_Connection=True;";
            
            using (var bd = new SqlBasedeDatos(cadenaConexion))
            {
                //ejemplo 1
                IEnumerable<Album> albums = new AlbumRepositorio(bd).Traer();

                albums
                    .Where(a => a.ArtistaId == 1)
                    .ToList()
                    .ForEach(r => Console.WriteLine($"{r.Titulo}"));

                Console.WriteLine(albums.Count(x => x.ArtistaId == 2));

                Console.WriteLine(albums.Max(x => x.ArtistaId));
                Console.WriteLine(albums.Sum(x => x.ArtistaId));
                Console.WriteLine(albums.Average(x => x.ArtistaId));
                Console.WriteLine(albums.Min(x => x.ArtistaId));

                Console.ReadKey();
                var consulta = from album in albums
                               where album.ArtistaId == 1
                               select new { AlbumId = album.Id, Nombre = album.Titulo };

                var resultado = albums.Select(x => new { x.Id, x.Titulo });

                resultado.ToList().ForEach(r => Console.WriteLine(r.Titulo));

            }


        }
    }
}
