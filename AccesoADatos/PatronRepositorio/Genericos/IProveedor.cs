﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatronRepositorio.Genericos
{
    class Entidad
    {

    }
    interface IProveedor<T> 
    {
        T Traer(int id);
        void Enviar(T mensaje);
    }

    interface IProveedorEntidades<T> where T : Entidad
    {
        T Traer(int id);
        void Enviar(T mensaje);

    }

    class ProvedorEntero : IProveedor<int>
    {
        public void Enviar(int mensaje)
        {
            throw new NotImplementedException();
        }

        public int Traer(int id)
        {
            throw new NotImplementedException();
        }
    }

    class Empleado : Entidad
    {

    }

    class ProveedorEntidadEmpleado : IProveedorEntidades<Empleado>
    {
        public void Enviar(Empleado mensaje)
        {
            throw new NotImplementedException();
        }

        public Empleado Traer(int id)
        {
            throw new NotImplementedException();
        }
    }

    class ProveedorEntidad : IProveedorEntidades<Entidad>
    {
        public void Enviar(Entidad mensaje)
        {
            throw new NotImplementedException();
        }

        public Entidad Traer(int id)
        {
            throw new NotImplementedException();
        }
    }
}
