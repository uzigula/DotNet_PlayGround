namespace Cliente.Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cursos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Semestre = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Estudiantes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Direccion = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EstudiantesCursos",
                c => new
                    {
                        Estudiantes_Id = c.Int(nullable: false),
                        Cursos_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Estudiantes_Id, t.Cursos_Id })
                .ForeignKey("dbo.Estudiantes", t => t.Estudiantes_Id, cascadeDelete: true)
                .ForeignKey("dbo.Cursos", t => t.Cursos_Id, cascadeDelete: true)
                .Index(t => t.Estudiantes_Id)
                .Index(t => t.Cursos_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EstudiantesCursos", "Cursos_Id", "dbo.Cursos");
            DropForeignKey("dbo.EstudiantesCursos", "Estudiantes_Id", "dbo.Estudiantes");
            DropIndex("dbo.EstudiantesCursos", new[] { "Cursos_Id" });
            DropIndex("dbo.EstudiantesCursos", new[] { "Estudiantes_Id" });
            DropTable("dbo.EstudiantesCursos");
            DropTable("dbo.Estudiantes");
            DropTable("dbo.Cursos");
        }
    }
}
