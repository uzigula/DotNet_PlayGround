namespace DapperMicro.Modelo
{
    public class MediaType
    { 
        public int MediaTypeId { get; set; }
        public string Name { get; set; }        
    }
}
