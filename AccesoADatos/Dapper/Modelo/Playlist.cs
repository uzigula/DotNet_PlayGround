namespace DapperMicro.Modelo
{
    public class Playlist
    {
        public int PlaylistId { get; set; }
        public string Name { get; set; }        
    }
}
