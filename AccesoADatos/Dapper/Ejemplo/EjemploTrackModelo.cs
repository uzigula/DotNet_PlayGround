﻿using DapperMicro.Modelo.VistaModelo;
using DapperMicro.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DapperMicro.Ejemplo
{
    public class EjemploTrackModelo
    {
        private IRepositorio<TrackModeloVista> repositorio;
        public EjemploTrackModelo()
        {
            repositorio = new RepositorioBase<TrackModeloVista>();
        }
        
        public IEnumerable<TrackModeloVista> ObtenerTrackConRelaciones()
        {            
            return repositorio.ObtenerProcedimiento("TrackConRelaciones", null);
        }
    }
}
