﻿using DapperMicro.Repositorio;
using DapperMicro.Modelo;
using System;
using System.Collections.Generic;
using DapperExtensions;
using DapperMicro.Modelo.VistaModelo;

namespace DapperMicro.Ejemplo
{
    public class EjemploCustomer
    {
        private IRepositorio<Customer> repositorio;
        public EjemploCustomer()
        {
            repositorio = new RepositorioBase<Customer>();
        }
        public int Crear(Customer customer)
        {
            return repositorio.Agregar(customer);
        }

        public void Actualizar(int id)
        {
            var customer = CustomerPorId(id);
            customer.Address = "Actualizando datos";
            customer.Phone = "99999999999";
            if (repositorio.Actualizar(customer))
                Console.WriteLine("La clase customer se ha actualizado corectamente.");
            else
                Console.WriteLine("No se ha actualizado ningun registro.");
        }

        public void Eliminar(int id)
        {
            var customer = CustomerPorId(id);
            if (repositorio.Eliminar(customer))
                Console.WriteLine($"El id: {id} se ha eliminado corectamente.");
            else
                Console.WriteLine("No se ha eliminado ningun registro.");
        }
        public Customer CustomerPorId(int id)
        {
            return repositorio.GetById(id);
        }

        public Customer Generate()
        {
            return new Customer
            {
                FirstName = "Juan",
                LastName = "Perez",
                Address = "Direccion de Test",
                City = "Lima",
                Country = "Peru",
                Email = "juvega@gmail.com",
                SupportRepId = 1
            };
        }

        public IEnumerable<Customer> Listado()
        {
            return repositorio.Listar();
        }

        public IEnumerable<Customer> ListadoPorConsulta(string consulta)
        {
            return repositorio.ListarPorConsulta(consulta);
        }

        public IEnumerable<Customer> FiltroPorPais(string pais)
        {
            var predicado = Predicates.Field<Customer>
                (
                x => x.Country, Operator.Like, pais
                );
            return repositorio.FiltroPorUnCampo(predicado);
        }

        public IEnumerable<Customer> FiltroPorPaisCompania(string pais, string compania)
        {
            var predicados = new PredicateGroup
            {
                Operator = GroupOperator.And,
                Predicates = new List<IPredicate>()
            };


            predicados.Predicates.Add(Predicates.Field<Customer>(x => x.Country, Operator.Eq, pais));
            predicados.Predicates.Add(Predicates.Field<Customer>(x => x.Company, Operator.Like, compania));

            return repositorio.FiltroVariosCampos(predicados);
        }

        public IEnumerable<Customer> FiltroCompuesto(string pais, string compania, string nombre, string apellido)
        {
            var predicados = new PredicateGroup
            {
                Operator = GroupOperator.Or,
                Predicates = new List<IPredicate>()
            };

            var predicadosTipoY = new PredicateGroup
            {
                Operator = GroupOperator.And,
                Predicates = new List<IPredicate>()
            };

            predicadosTipoY.Predicates.Add(Predicates.Field<Customer>(x => x.Country, Operator.Eq, pais));
            predicadosTipoY.Predicates.Add(Predicates.Field<Customer>(x => x.Company, Operator.Like, compania));
            predicados.Predicates.Add(predicadosTipoY);

            var predicadosTipoO = new PredicateGroup
            {
                Operator = GroupOperator.Or,
                Predicates = new List<IPredicate>()
            };

            predicadosTipoO.Predicates.Add(Predicates.Field<Customer>(x => x.FirstName, Operator.Like, nombre));
            predicadosTipoO.Predicates.Add(Predicates.Field<Customer>(x => x.LastName, Operator.Like, apellido));
            predicados.Predicates.Add(predicadosTipoO);

            return repositorio.FiltroVariosCampos(predicados);
        }


        public IEnumerable<Customer> ObtenerProcedimiento()
        {            
            return repositorio.ObtenerProcedimiento("ObtenerCustomerPorNombre", new { FirstName="ro" });
        }

    }
}
