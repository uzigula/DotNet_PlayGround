﻿using DapperMicro.Ejemplo;
using DapperMicro.Modelo;
using DapperMicro.Modelo.VistaModelo;
using System;
using System.Collections.Generic;

namespace Dapper
{
    class Program
    {
        static void Main(string[] args)
        {
            ProcesarCustomer();
            Console.ReadKey();
        }        

        private static void ProcesarCustomer()
        {
            var customerTest = new EjemploCustomer();
            Console.WriteLine("Creando Customer");
            var llavePrimaria = customerTest.Crear(customerTest.Generate());
            if (llavePrimaria > 0)
            {
                Console.WriteLine($"La clase customer sea ha creado corectamente con el ID: {llavePrimaria}");

                Console.WriteLine("Actualizando Customer");
                var customerPorActualizar = customerTest.CustomerPorId(llavePrimaria);
                ImprimirCustomer(customerPorActualizar);
                PresionarParaContinuar();
                Console.ReadKey();
                customerTest.Actualizar(llavePrimaria);
                var customerActualizado = customerTest.CustomerPorId(llavePrimaria);
                ImprimirCustomer(customerActualizado);
                PresionarParaContinuar();
                Console.ReadKey();

                Console.WriteLine("Eliminando Customer");
                customerTest.Eliminar(llavePrimaria);
                PresionarParaContinuar();

            }
            else
            {
                Console.WriteLine("No se ha agregado ningun registro.");
                PresionarParaContinuar();
            }

            Console.ReadKey();
            Console.WriteLine("Listado de Customer por consulta");
            var consulta = @"SELECT [CustomerId]
                                  ,[FirstName]
                                  ,[LastName]
                                  ,[Company]
                                  ,[Address]
                                  ,[City]
                                  ,[State]
                                  ,[Country]
                                  ,[PostalCode]
                                  ,[Phone]
                                  ,[Fax]
                                  ,[Email]
                                  ,[SupportRepId]
                              FROM [Chinook].[dbo].[Customer]";
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var listadoPorConsulta = customerTest.ListadoPorConsulta(consulta);            
            watch.Stop();
            ImprimirListaCustomer(listadoPorConsulta);
            Console.WriteLine($"Tiempo de Ejecucion: {watch.ElapsedMilliseconds} ms");
            Console.ReadKey();

            Console.WriteLine("Listado de Customer por Dapper Extenssion");
            watch.Restart();
            var listadoPorExtension = customerTest.Listado();
            watch.Stop();
            ImprimirListaCustomer(listadoPorConsulta);
            Console.WriteLine($"Tiempo de Ejecucion: {watch.ElapsedMilliseconds} ms");
            Console.ReadKey();

            Console.WriteLine("Listado de Customer por Predicado");
            watch.Restart();
            var listadoPorPais = customerTest.FiltroPorPais("%usa%");
            watch.Stop();
            ImprimirListaCustomer(listadoPorPais);
            Console.WriteLine($"Tiempo de Ejecucion: {watch.ElapsedMilliseconds} ms");

            Console.ReadKey();
            Console.WriteLine("Listado de Customer por Predicado 2 campos");
            watch.Restart();
            var listadoPorPaisCompania = customerTest.FiltroPorPaisCompania("usa", "%goo%");
            watch.Stop();
            ImprimirListaCustomer(listadoPorPaisCompania);
            Console.WriteLine($"Tiempo de Ejecucion: {watch.ElapsedMilliseconds} ms");

            Console.ReadKey();
            Console.WriteLine("Listado de Customer por Predicado Compuesto");
            watch.Restart();
            var listadoCompuesto = customerTest.FiltroCompuesto("usa", "%goo%", "%fr%", "%har%");
            watch.Stop();
            ImprimirListaCustomer(listadoCompuesto);
            Console.WriteLine($"Tiempo de Ejecucion: {watch.ElapsedMilliseconds} ms");

            Console.ReadKey();
            Console.WriteLine("Listado de Customer por Procedimiento Almacenado");
            watch.Restart();
            var listadoProcedimiento = customerTest.ObtenerProcedimiento();
            watch.Stop();
            ImprimirListaCustomer(listadoProcedimiento);
            Console.WriteLine($"Tiempo de Ejecucion: {watch.ElapsedMilliseconds} ms");
            Console.ReadKey();
            
            Console.WriteLine("Listado de Track con relaciones por Procedimiento Almacenado");
            var trackTest = new EjemploTrackModelo();
            watch.Restart();
            var listadoTrack = trackTest.ObtenerTrackConRelaciones();
            watch.Stop();
            ImprimirListaTrack(listadoTrack);
            Console.WriteLine($"Tiempo de Ejecucion: {watch.ElapsedMilliseconds} ms");

        }
        private static void PresionarParaContinuar()
        {
            Console.WriteLine("Presiona una tecla para continuar");
        }

        private static void ImprimirCustomer(Customer customer)
        {
            Console.WriteLine($"CustomerId: {customer.CustomerId}");
            Console.WriteLine($"FirstName: {customer.FirstName}");
            Console.WriteLine($"LastName: {customer.LastName}");
            Console.WriteLine($"Company: {customer.Company}");
            Console.WriteLine($"Address: {customer.Address}");
            Console.WriteLine($"City: {customer.City}");
            Console.WriteLine($"State: {customer.State}");
            Console.WriteLine($"Country: {customer.Country}");
            Console.WriteLine($"PostalCode: {customer.PostalCode}");
            Console.WriteLine($"Phone: {customer.Phone}");
            Console.WriteLine($"Fax: {customer.Fax}");
            Console.WriteLine($"Email: {customer.Email}");
            Console.WriteLine($"SupportRepId: {customer.SupportRepId}");            
        }

        private static void ImprimirListaCustomer(IEnumerable<Customer> lista)
        {
            Console.WriteLine($"CustomerId\t\tFirstName\t\tLastName\t\tCountry\t\tCompany");
            foreach (var customer in lista)
            {                
                Console.WriteLine($"{customer.CustomerId}\t\t{customer.FirstName}\t\t{customer.LastName}\t\t{customer.Country}\t\t{customer.Company}");
            }            
        }

        private static void ImprimirListaTrack(IEnumerable<TrackModeloVista> lista)
        {
            Console.WriteLine("TrackId\tName\tTitle\tMediaName\tGenreName\tComposer\tMilliseconds\tBytes\tUnitPrice");
            foreach (var track in lista)
            {
                Console.WriteLine($"{track.TrackId}\t{track.Name}\t{track.Title}\t{track.MediaName}\t{track.GenreName}\t{track.Composer}\t{track.Milliseconds}\t{track.Bytes}\t{track.UnitPrice}");
            }
        }

    }
}
