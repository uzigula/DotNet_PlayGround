## Dapper - a simple object mapper for .Net

Dapper es una librería NuGet que extiende la funcionalidad de la interfaz IDbConnection.

Para hacer uso de esta librería necesitamos instalar el siguiente paquete:

    PM> Install-Package Dapper

Dapper nos permite realizar queries de manera rapida, pero para realizar un CRUD básico necesitamos una extensión de Dapper llamada **Dapper-Extensions**

    PM> Install-Package DapperExtensions

## Dapper Extensions
Esta librería nos permite la utilizacion de métodos para poder implementar el mantenimiento de una tabla.
Para hacer uso de esta extension primero se tiene que definir una variable del tipo: **SqlConnection** y es a esa variable 

    var conexionSql = new SqlConnection(cadenaConexion)
1. **Extensión Add**: 

        conexionSql.Insert({Objeto a Insertar} )
    
2. **Extensión Update**:

        conexionSql.Update({Objeto a Actualizar} )
    
3. **Extensión Delete**:

        conexionSql.Delete({Objeto a Actualizar} )
    
4. **Extensión Get<T>**:

        conexionSql.Get<T>({Llave primaria} )
    
5. **Extensión GetList<T>**: Devuelve la lista de elementos del objeto T indicado.


        conexionSql.GetList<T>()
    
