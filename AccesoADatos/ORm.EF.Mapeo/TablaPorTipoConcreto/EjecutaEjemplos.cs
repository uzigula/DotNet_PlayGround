﻿using ORm.EF.Mapeo.TablaPorTipoConcreto.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORm.EF.Mapeo.TablaPorTipoConcreto
{
    class EjecutaEjemplos
    {
        static void Main(string[] args)
        {
            CreaContratos();
            ConsultaTablaPorJerarquia();
        }

        private static void ConsultaTablaPorJerarquia()
        {
            using (var bd = new ContextoTablaPorTipoConcreto())
            {
                Console.WriteLine($"Todos los contratos: {bd.Contratos.Count()}");
                Console.WriteLine($"A Credito {bd.Contratos.OfType<ContratoCredito>().Count()}");
                Console.WriteLine($"Prepago {bd.Contratos.OfType<ContratoPrepago>().Count()}");
            }
        }

        private static void CreaContratos()
        {
            using (var bd = new ContextoTablaPorTipoConcreto())
            {
                ContratoCredito contrato1 = new ContratoCredito()
                {
                    Inicio = DateTime.Now,
                    Finaliza = DateTime.Now.AddMonths(12),
                    Limite = 1000,
                    Moneda = "PEN",
                };

                bd.Contratos.Add(contrato1);


                ContratoPrepago contrato2 = new ContratoPrepago()
                {
                    Inicio = DateTime.Now,
                    Finaliza = DateTime.Now.AddMonths(12),
                    MontoPrepago = 500,
                    PeriodoPrepago = "M"
                };

                bd.Contratos.Add(contrato2);

                bd.SaveChanges();
            }
        }
    }
}
