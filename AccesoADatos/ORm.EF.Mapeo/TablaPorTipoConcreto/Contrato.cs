﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ORm.EF.Mapeo.TablaPorTipoConcreto.Modelo
{
    public abstract class Contrato
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)] // solo para SQL server
        public Guid Id { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public DateTime Inicio { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public DateTime Finaliza { get; set; }
    }

    public class ContratoCredito : Contrato
    {
        [Required]
        public float Limite { get; set; }

        [Column(TypeName = "char")]
        [StringLength(3)]
        [Required]
        public string Moneda { get; set; }
    }

    public class ContratoPrepago : Contrato
    {
        [Required]
        public float MontoPrepago { get; set; }

        [Column(TypeName = "char")]
        [StringLength(1)]
        [Required]
        public string PeriodoPrepago { get; set; }
    }
}
