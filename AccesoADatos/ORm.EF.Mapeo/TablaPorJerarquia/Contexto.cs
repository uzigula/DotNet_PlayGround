﻿using ORm.EF.Mapeo.TablaPorJerarquia.Modelo;
using System.Data.Entity;

namespace ORm.EF.Mapeo.TablaPorJerarquia
{
    class ContextoTablaPorJerarquia : DbContext
    {
        // creo un DbSet para mi Jerarquia
        public DbSet<Contrato> Contratos { get; set; }
        public ContextoTablaPorJerarquia() 
            : base("name=PorJerarquia")
        {

        }

        /// <summary>
        /// aqui defino como se mapeara mi jerarqui en la bd
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // mapeo todos los hijos de la Jearquia
            modelBuilder.Entity<Contrato>()
                .Map<ContratoCredito>(m => m.Requires("Discriminador").HasValue("C"))
                .Map<ContratoPrepago>(m => m.Requires("Discriminador").HasValue("P"));

            base.OnModelCreating(modelBuilder);
        }
    }
}
