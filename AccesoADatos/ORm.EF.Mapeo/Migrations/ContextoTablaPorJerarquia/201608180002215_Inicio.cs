namespace ORm.EF.Mapeo.Migrations.ContextoTablaPorJerarquia
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contratoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Inicio = c.DateTime(nullable: false),
                        Finaliza = c.DateTime(nullable: false),
                        Limite = c.Single(),
                        Moneda = c.String(maxLength: 3, fixedLength: true, unicode: false),
                        MontoPrepago = c.Single(),
                        PeriodoPrepago = c.String(maxLength: 1, fixedLength: true, unicode: false),
                        Discriminador = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Contratoes");
        }
    }
}
