namespace ORm.EF.Mapeo.Migrations.ContextoTablaPorTipo
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class clientes : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Contratoes", newName: "Contrato");
            CreateTable(
                "dbo.Cliente",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ContratoCliente",
                c => new
                    {
                        Contrato_Id = c.Int(nullable: false),
                        Cliente_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Contrato_Id, t.Cliente_Id })
                .ForeignKey("dbo.Contrato", t => t.Contrato_Id, cascadeDelete: true)
                .ForeignKey("dbo.Cliente", t => t.Cliente_Id, cascadeDelete: true)
                .Index(t => t.Contrato_Id)
                .Index(t => t.Cliente_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContratoCliente", "Cliente_Id", "dbo.Cliente");
            DropForeignKey("dbo.ContratoCliente", "Contrato_Id", "dbo.Contrato");
            DropIndex("dbo.ContratoCliente", new[] { "Cliente_Id" });
            DropIndex("dbo.ContratoCliente", new[] { "Contrato_Id" });
            DropTable("dbo.ContratoCliente");
            DropTable("dbo.Cliente");
            RenameTable(name: "dbo.Contrato", newName: "Contratoes");
        }
    }
}
