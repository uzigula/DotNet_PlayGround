namespace ORm.EF.Mapeo.Migrations.ContextoTablaPorTipoConcreto
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ORm.EF.Mapeo.TablaPorTipoConcreto.ContextoTablaPorTipoConcreto>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations\ContextoTablaPorTipoConcreto";
        }

        protected override void Seed(ORm.EF.Mapeo.TablaPorTipoConcreto.ContextoTablaPorTipoConcreto context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
