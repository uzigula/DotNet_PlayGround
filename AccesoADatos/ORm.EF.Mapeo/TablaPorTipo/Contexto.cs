﻿using ORm.EF.Mapeo.TablaPorTipo.Modelo;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ORm.EF.Mapeo.TablaPorTipo
{
    class ContextoTablaPorTipo : DbContext
    {
        // creo un DbSet para mi Jerarquia
        public DbSet<Contrato> Contratos { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public ContextoTablaPorTipo() 
            : base("name=TablaPorTipo")
        {

        }

        /// <summary>
        /// aqui defino como se mapeara mi jerarqui en la bd
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            // mapeo tabla por tipo 
            modelBuilder.Entity<ContratoCredito>()
                .Map(m =>
                {
                    m.ToTable("ContratoCredito");
                }
                );

            modelBuilder.Entity<ContratoPrepago>()
                .Map(m=>
                {
                    m.ToTable("ContratoPrepago");
                }
                );
            
            base.OnModelCreating(modelBuilder);
        }
    }
}
