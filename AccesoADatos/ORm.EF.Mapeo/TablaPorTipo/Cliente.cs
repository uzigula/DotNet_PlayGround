﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORm.EF.Mapeo.TablaPorTipo.Modelo
{
    public class Cliente : Entidad
    {
        public Cliente()
        {
            this.Contratos = new HashSet<Contrato>();
        }
        public string Nombre { get; set; }

        public ICollection<Contrato> Contratos { get; set; }
    }
}
