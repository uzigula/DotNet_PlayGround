﻿using ORm.EF.Mapeo.TablaPorTipo.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORm.EF.Mapeo.TablaPorTipo
{
    class EjecutaEjemplos
    {
        static void Main(string[] args)
        {
            CreaContratos();
            ConsultaTablaPorJerarquia();
        }

        private static void ConsultaTablaPorJerarquia()
        {
            using (var bd = new ContextoTablaPorTipo())
            {
                Console.WriteLine($"Todos los contratos: {bd.Contratos.Count()}");
                Console.WriteLine($"A Credito {bd.Contratos.OfType<ContratoCredito>().Count()}");
                Console.WriteLine($"Prepago {bd.Contratos.OfType<ContratoPrepago>().Count()}");
            }
        }

        private static void CreaContratos()
        {
            using (var bd = new ContextoTablaPorTipo())
            {
                ContratoCredito contrato1 = new ContratoCredito()
                {
                    Inicio = DateTime.Now,
                    Finaliza = DateTime.Now.AddMonths(12),
                    Limite = 1000,
                    Moneda = "PEN",
                };

                bd.Contratos.Add(contrato1);


                ContratoPrepago contrato2 = new ContratoPrepago()
                {
                    Inicio = DateTime.Now,
                    Finaliza = DateTime.Now.AddMonths(12),
                    MontoPrepago = 500,
                    PeriodoPrepago = "M"
                };

                bd.Contratos.Add(contrato2);

                bd.SaveChanges();

                Cliente cliente = new Cliente
                {
                    Nombre = "Uzi Mamani",
                    Contratos = new List<Contrato>
                    {
                        contrato1,
                        contrato2
                    }
                };

                bd.Clientes.Add(cliente);
                bd.SaveChanges();
            }
        }
    }
}
