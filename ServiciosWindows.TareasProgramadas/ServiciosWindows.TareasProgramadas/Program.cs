﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace ServiciosWindows.TareasProgramadas
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(host =>
                {
                    host.Service<Servicio>(servicio =>
                      {
                          servicio.ConstructUsing(() => new Servicio());
                          servicio.WhenStarted((svc, hostControl) => svc.Start(hostControl));
                          servicio.WhenStopped((svc, hostControl) => svc.Stop(hostControl));
                          servicio.WhenShutdown((svc, hostControl) => svc.Stop(hostControl));
                          servicio.WhenPaused((svc, hostControl) => svc.Pause(hostControl));
                          servicio.WhenContinued((svc, hostControl) => svc.Continue(hostControl));
                      });
                    host.SetServiceName("ServicioPruebaTareas");
                    host.SetDisplayName("CRM Servicio de Pruebas para Tareas Programadas");
                    host.SetDescription("Servicio que ejecuta tareas programadas de actualizacion de la base de datos de reportes");
                });
        }
    }
}
