﻿using Quartz;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServiciosWindows.TareasProgramadas
{
    class ConteoProductosJob : BaseJob, IJob
    {

        public void Execute(IJobExecutionContext context)
        {

            if (EsteJobSeEstaEjecutando(context, 
                () => { Console.WriteLine($"{DateTime.Now.ToLongTimeString()} ConteoProductosJob is curently executing"); }))
                    return;

            // se pone el codigo que se ejecuta cuando el trigger se lanze

            LogMensaje("Se Ejecuto El Job ConteoProductosJob");

            Thread.Sleep(30000);
            LogMensaje("Se Termino El Job ConteoProductosJob");

        }
    }
}
