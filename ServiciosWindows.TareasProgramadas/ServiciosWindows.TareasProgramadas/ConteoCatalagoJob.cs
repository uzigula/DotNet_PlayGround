﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServiciosWindows.TareasProgramadas
{
    class ConteoCatalagoJob : BaseJob, IJob
    {
        public void Execute(IJobExecutionContext context)
        {

            if (EsteJobSeEstaEjecutando(context,
                () => { Console.WriteLine($"{DateTime.Now.ToLongTimeString()} ConteoCatalagoJob is curently executing"); }))
                return;

            // se pone el codigo que se ejecuta cuando el trigger se lanze
            var guardarlog = ConfigurationManager.AppSettings["GuardarLog"];

            var navidad = Properties.Settings.Default.Navidad;
            var igv = Properties.Settings.Default.Igv;
            var valor = AW.SalesApp.Data.Properties.Settings.Default.DataModel;
            LogMensaje($"{valor}");
            LogMensaje($"El valor de l clave es: {100*igv}");
            LogMensaje($"El valor de l clave es: {guardarlog}");
            LogMensaje("Se Ejecuto El Job ConteoCatalogo Job");

            Thread.Sleep(30000);
            LogMensaje("Se Termino El Job ConteoCatalogo");

        }


    }
}
