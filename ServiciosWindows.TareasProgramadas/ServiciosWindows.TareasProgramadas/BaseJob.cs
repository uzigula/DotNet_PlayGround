﻿using Quartz;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ServiciosWindows.TareasProgramadas
{
    abstract class BaseJob
    {
        private readonly string _archivoLog;

        protected BaseJob()
        {
            _archivoLog = ArchivoLog();
        }
        protected bool EsteJobSeEstaEjecutando(IJobExecutionContext context, Action action)
        {
            var flag = context.Scheduler
                            .GetCurrentlyExecutingJobs()
                            .Any(x => x.FireInstanceId != context.FireInstanceId
                            && x.JobDetail.Key == context.JobDetail.Key);

            if (flag) action();

            return flag;
        }

        protected void LogMensaje(string mensaje)
        {
            using (var fs = File.Open(_archivoLog, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.AutoFlush = true;
                writer.WriteLine($"{DateTime.Now.ToLongTimeString()} {mensaje}");
            }
        }


        private string ArchivoLog()
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            return Path.Combine(Path.GetDirectoryName(path),"Audit.log");

        }
    }
}
