﻿using Quartz;
using Quartz.Impl;
using Quartz.Impl.Triggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Topshelf;

namespace ServiciosWindows.TareasProgramadas
{
    public class Servicio : ServiceControl
    {
        private IScheduler programadorDeTareas;

        public bool Start(HostControl hostControl)
        {
            Console.WriteLine($"Se inicio el servicio");
            // codigo de inicializacion del servicio
            ConfigurarTareasProgramadas();
            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            Console.WriteLine($"Se detuvo el servicio");
            // codigo de cierre del servicio
            return true;
        }

        public bool Pause(HostControl hostControl)
        {
            Console.WriteLine($"Se pauso el servicio ");
            // codigo de cierre del servicio
            return true;

        }

        public bool Continue(HostControl hostControl)
        {
            Console.WriteLine($"Se continua ejecutando el servicio ");
            // codigo de cierre del servicio
            return true;

        }

        private void ConfigurarTareasProgramadas()
        {
            // creamos el Programador de tareas
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            programadorDeTareas = schedulerFactory.GetScheduler();
            programadorDeTareas.Start();

            // crear un Job
            ConfigurarJob<ConteoProductosJob>("0/15 * * * * ?");
            ConfigurarJob<ConteoCatalagoJob>("0/15 * * * * ?"); 
        }

        private void ConfigurarJob<TJob>(string expresionCron) where TJob:IJob
        {
            var tipo = typeof(TJob);
            var cronTrigger = new CronTriggerImpl(tipo.Name,
                $"{tipo.Name} Grupo",
                expresionCron
                );

            var jobDetail = new JobDetailImpl(tipo.Name,
                $"{tipo.Name} Grupo",
                tipo
                );
            programadorDeTareas.ScheduleJob(jobDetail, cronTrigger);
        }
    }
}
