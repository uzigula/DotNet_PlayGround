﻿using AW.SalesApp.Model;
using System.Data.Entity;
using System;

namespace AW.SalesApp.Data
{
    public class AwRepositorio
    {
        private readonly DbContext bd;

        private readonly RepositorioGenerico<Product> _producto;
        private readonly RepositorioGenerico<ProductCategory> _categoriaProducto;
        private readonly RepositorioGenerico<ProductModel> _modeloProducto;
        private readonly RepositorioGenerico<UnitMeasure> _unidadMedida;

        public AwRepositorio(AwContext bd)
        {
            this.bd = bd;
            _producto = new RepositorioGenerico<Product>(bd);
            _categoriaProducto = new RepositorioGenerico<ProductCategory>(bd);
            _modeloProducto = new RepositorioGenerico<ProductModel>(bd);
            _unidadMedida = new RepositorioGenerico<UnitMeasure>(bd);
        }

        public RepositorioGenerico<Product> Productos { get { return _producto; } }
        public RepositorioGenerico<ProductCategory> Categorias { get { return _categoriaProducto; } }
        public RepositorioGenerico<ProductModel> Modelos { get { return _modeloProducto; } }

        public void Commit()
        {
            bd.SaveChanges();
        }

        public RepositorioGenerico<UnitMeasure> UnidadDeMedida { get { return _unidadMedida; } }
    }
}