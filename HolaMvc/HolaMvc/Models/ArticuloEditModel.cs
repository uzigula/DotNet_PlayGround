﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HolaMvc.Models
{
    public class ArticuloEditModel
    {
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        [DisplayName("Unid. Medida")]
        public string UnidadDeMedida { get; set; }

        [Required]
        public string Ubicación { get; set; }

        [Required]
        public decimal Precio { get; set; }

    }
}