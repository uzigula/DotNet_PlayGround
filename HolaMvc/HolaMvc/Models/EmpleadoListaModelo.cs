﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HolaMvc.Models
{
    public class EmpleadoListaModelo
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }

        [DisplayName("Nacido en")]
        [DataType(DataType.Date)]
        public DateTime NacidoEn { get; set; }

        public string Nacionalidad { get; set; }

        [DataType(DataType.EmailAddress)]
        [DisplayName("Email")]
        public string CorreoElectronico { get; set; }

    }
}
