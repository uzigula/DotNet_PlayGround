﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HolaMvc.Models
{
    public class EmpleadoEditModel
    {
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        public string Apellidos { get; set; }

        [Required]
        [DisplayName("Nacido en")]
        [DataType(DataType.Date)]
        public DateTime NacidoEn { get; set; }

        [Required]
        public string Nacionalidad { get; set; }

        [Required]
        [DisplayName("Dirección Fiscal")]
        public string DireccionFiscal { get; set; }

        [Required]
        public decimal Sueldo { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email")]
        public string CorreoElectronico { get; set; }

        public EmpleadoEditModel()
        {
        }
    }
}