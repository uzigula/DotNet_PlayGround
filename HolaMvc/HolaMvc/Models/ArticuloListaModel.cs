﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HolaMvc.Models
{
    public class ArticuloListaModel
    {
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }
        [DisplayName("Unid. Medida")]

        public string Ubicación { get; set; }

        public decimal Precio { get; set; }

    }
}