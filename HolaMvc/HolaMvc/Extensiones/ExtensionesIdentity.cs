﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.AspNet.Identity
{
    public static class ExtensionesIdentity
    {
        public static string ObtenerNombre(this IPrincipal user)
        {
            if (!user.Identity.IsAuthenticated) return string.Empty;

            var claims = user.Identity as ClaimsIdentity;

            if (claims == null) return string.Empty;

            var nombre = claims.Claims.SingleOrDefault(x => x.Type == "Nombre");


            return nombre==null ? string.Empty : nombre.Value;

        }
    }
}
