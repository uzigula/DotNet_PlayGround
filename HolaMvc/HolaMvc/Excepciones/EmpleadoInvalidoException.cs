﻿using System;
using System.Runtime.Serialization;

namespace System
{
    [Serializable]
    class EmpleadoInvalidoException : Exception
    {
        public EmpleadoInvalidoException()
        {
        }

        public EmpleadoInvalidoException(string message) : base(message)
        {
        }

        public EmpleadoInvalidoException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EmpleadoInvalidoException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}