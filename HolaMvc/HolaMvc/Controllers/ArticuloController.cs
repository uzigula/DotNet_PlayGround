﻿using HolaMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HolaMvc.Controllers
{
    public class ArticuloController : Controller
    {
        static List<ArticuloEditModel> articulos;

        public ArticuloController()
        {
            if (articulos == null) articulos =  new List<ArticuloEditModel>();
        }

        [HttpGet]
        public ActionResult Nuevo()
        {
            return View(new ArticuloEditModel());
        }

        [HttpPost]
        public ActionResult Nuevo(ArticuloEditModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);
            try
            {
                // se procesa la grabacion
                modelo.Id = articulos.Count + 1;
                articulos.Add(modelo);
                return RedirectToAction("Lista");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(modelo);
            }

        }

        public ActionResult Lista()
        {
            //codigo que trae la lista
            var lista = articulos.Select(x => new ArticuloListaModel()
            {
                Id = x.Id,
                Nombre = x.Nombre,
                Ubicación = x.Ubicación,
                Precio = x.Precio
            });
            return View(lista);
        }


        public ActionResult Ver(int id)
        {
            return View(articulos.Single(x => x.Id == id));
        }

        public ActionResult Editar(int id)
        {
            return View(articulos.Single(x => x.Id == id));
        }

        [HttpPost]
        public ActionResult Editar(ArticuloEditModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);
            try
            {
                // se procesa la grabacion
                var articulo = articulos.Single(x => x.Id == modelo.Id);
                articulos.Remove(articulo);
                articulos.Add(modelo);
                return RedirectToAction("Lista");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(modelo);
            }

        }
        [HttpGet]
        public ActionResult Eliminar(int id)
        {
            return View(articulos.Single(x => x.Id == id));
        }

        [HttpPost]
        public ActionResult Eliminar(int id, string nombre)
        {
            try
            {
                // se procesa la grabacion
                var articulo = articulos.Single(x => x.Id == id);
                articulos.Remove(articulo);
                return RedirectToAction("Lista");
            }
            catch (Exception ex)
            {
                return View(articulos.Single(x => x.Id == id));
            }

        }

    }
}