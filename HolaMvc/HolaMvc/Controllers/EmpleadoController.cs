﻿using HolaMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HolaMvc.Controllers
{

    [Authorize(Roles = "Admin")]
    public class EmpleadoController : Controller
    {
        static List<EmpleadoEditModel> empleados;


        public EmpleadoController()
        {
            if (empleados == null) empleados =  new List<EmpleadoEditModel>();
        }
        // GET: Empleado
        [HttpGet]
        public ActionResult Nuevo()
        {
            return View(new EmpleadoEditModel());
        }

        [HttpPost]
        public ActionResult Nuevo(EmpleadoEditModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);
            try
            {
                if (modelo.Sueldo <= 0) throw new EmpleadoInvalidoException("Sueldo debe ser mayor que Cero");
                // se procesa la grabacion
                modelo.Id = empleados.Count + 1;
                empleados.Add(modelo);
                return RedirectToAction("Lista");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(modelo);
            }

       }

        [AllowAnonymous]
        public ActionResult Lista()
        {
            //codigo que trae la lista
            var lista = empleados.Select(x => new EmpleadoListaModelo()
            {
                Id = x.Id,
                Nombre = x.Nombre,
                Apellidos = x.Apellidos,
                NacidoEn = x.NacidoEn,
                Nacionalidad = x.Nacionalidad,
                CorreoElectronico = x.CorreoElectronico
            });
            return View(lista);
        }


        public ActionResult Ver(int id)
        {
            return View(empleados.Single(x => x.Id == id));
        }

        public ActionResult Editar(int id)
        {
            return View(empleados.Single(x => x.Id == id));
        }

        [HttpPost]
        public ActionResult Editar(EmpleadoEditModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);
            try
            {
                if (modelo.Sueldo <= 0) throw new EmpleadoInvalidoException("Sueldo debe ser mayor que Cero");
                // se procesa la grabacion
                var empleado = empleados.Single(x => x.Id == modelo.Id);
                empleados.Remove(empleado);
                empleados.Add(modelo);
                return RedirectToAction("Lista");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(modelo);
            }

        }
        [HttpGet]
        public ActionResult Eliminar(int id)
        {
            return View(empleados.Single(x => x.Id == id));
        }

        [HttpPost]
        public ActionResult Eliminar(int id, string nombre)
        {
            try
            {
                // se procesa la grabacion
                var empleado = empleados.Single(x => x.Id == id);
                empleados.Remove(empleado);
                return RedirectToAction("Lista");
            }
            catch (Exception ex)
            {
                return View(empleados.Single(x => x.Id == id));
            }

        }

    }
}