# Codigo de ejemplo Programación .Net

## Contenido

1. [El Repositorio](#repositorio)
1. Introducción  
1. [Linq](AccesoADatos/IntroLinq/Leeme.md)
1. [Acceso a a datos](AccesoADatos/Intro/Leeme.md)
1. Patron Repositorio
1. ORM
1. [Entity Framework](AccesoADatos/Orm.EF/Leeme.md)
1. [Dapper](AccesoADatos/Dapper/Leeme.md)




## Repositorio

Para este curso necesitan descargar e instalar [git] para windows

una vez instalado en su sistema operativo

pueden clonar el repositorio del desde la ventana de comandos

	c:\codigo>git clone https://gitlab.com/uzigula/DotNet_PlayGround.git

Cada Avance del curso fue etiquetado (Tag) de forma que pueden ver el estado del codigo paso a paso.

Para ver las etiquetas disponibles desde la ventana de comandos ejecute:


	c:\codigo\DotNet_PlayGround>git tag


Luego para poder ver el estado del codigo fuente en un determinado momento ejecute:

	c:\codigo\DotNet_PlayGround>git checkout [Etiqueta]


[Git]: <https://git-scm.com/download/win>