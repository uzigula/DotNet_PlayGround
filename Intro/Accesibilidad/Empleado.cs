﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accesibilidad
{
    class Empleado
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
    }

    class Funcionario
    {
        public Funcionario(string nombre, string apellido)
        {
            Nombre = nombre;
            Apellido = apellido;
        }
        public string Nombre { get; private set; }
        public string Apellido { get; private set; }

    }
}
