﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accesibilidad
{
    class Program
    {
        static void Main(string[] args)
        {
            Empleado empleado = new Empleado { Nombre = "Juan", Apellido = "Perez"};
            Console.WriteLine($"Empleado {empleado.Nombre} {empleado.Apellido}");

            Funcionario funcionario = new Funcionario("Juan", "Perez");
            Console.WriteLine($"Empleado {funcionario.Nombre} {funcionario.Apellido}");

            Console.Read();
        }
    }
}
