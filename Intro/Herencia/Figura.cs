﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    abstract class Figura
    {
        public Figura()
        {
            Etiquetas = new List<string>();
        }
        protected List<string> Etiquetas;
        public abstract void Dibujar();

        public void AgregarEtiqueta(string etiqueta)
        {
            Etiquetas.Add(etiqueta);
        }

        public abstract void MostrarEtiquetas(Punto posicion);
    }

    class Punto
    {
        public Punto(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public int X { get; private set; }
        public int Y { get; private set; }
    }

    class Circulo : Figura
    {
        public Circulo(Punto centro, decimal radio)
        {
            this.Centro = centro;
            this.Radio = radio;
        }

        public Punto Centro { get; private set; }
        public decimal Radio { get; private set; }

        public override void Dibujar()
        {
            // poner el codigo para dibujar el circulo
            Console.WriteLine(
                $"Dibujando un Circulo Centro {Centro.X},{Centro.Y} Radio {Radio}"
                );
        }

        public override void MostrarEtiquetas(Punto posicion)
        {
            Console.WriteLine("Etiquetas del Circulo");
            foreach (var etiqueta in Etiquetas)
            {
                Console.WriteLine($"{etiqueta}");
            }
        }
    }

    class Cuadrilatero : Figura
    {
        public Cuadrilatero(Punto esquinaSuperiorIzquierda, decimal altura, decimal ancho)
        {
            this.Altura = altura;
            this.Ancho = ancho;
            this.EsquinaSuperiorIzquierda = esquinaSuperiorIzquierda;
        }

        public decimal Altura { get; private set; }
        public decimal Ancho { get; private set; }
        public Punto EsquinaSuperiorIzquierda { get; private set; }

        public override void Dibujar()
        {
            Console.WriteLine(
                $"Dibujando un Cuadrilatero Esq. Sup. Izq. {EsquinaSuperiorIzquierda.X},{EsquinaSuperiorIzquierda.Y} Altura {Altura} Ancho {Ancho}"
                );
        }

        public override void MostrarEtiquetas(Punto posicion)
        {
            Console.WriteLine("Etiquetas del Cuadrilatero");
            foreach (var etiqueta in Etiquetas)
            {
                Console.WriteLine($"{etiqueta}");
            }

        }
    }
}
