﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notificaciones
{
    class NotificacionTwitter :INotificacion
    {
        private string Password;

        public NotificacionTwitter(string cuenta, string password, string mensaje)
        {
            this.Cuenta = cuenta;
            this.Password = password;
            this.Mensaje = mensaje;
        }

        private string Cuenta;
        private string Mensaje;

        public void Enviar()
        {
            Console.WriteLine($"Enviando twit desde cuenta {Cuenta}");
            Console.WriteLine($"mensaje: {Mensaje}");
        }
    }


}
