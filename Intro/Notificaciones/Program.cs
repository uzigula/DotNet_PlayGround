﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notificaciones
{
    class Program
    {
        static void Main(string[] args)
        {
            List<INotificacion> notificaciones = new List<INotificacion>();


            NotificacionSms notificacionSMS = new NotificacionSms();
            notificacionSMS.AgregarDestinatario("999123456");
            notificacionSMS.Mensaje = "Un ejemplo de Herencia con Interfaces";

            notificaciones.Add(notificacionSMS);

            NotificacionTwitter twit = new NotificacionTwitter("@usuario", "password", "Este es un Twit");


            notificaciones.Add(twit);

            Notificar(notificaciones);

            Console.ReadLine();
        }

        private static void Notificar(List<INotificacion> notificaciones)
        {
            notificaciones.ForEach(notif => notif.Enviar());
        }
    }
}
