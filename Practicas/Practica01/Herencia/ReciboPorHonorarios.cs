﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class ReciboPorHonorarios : Comprobante
    {
        public ReciboPorHonorarios(DateTime emitidoEn, string ruc, string razonSocial)
        {
            EmitidoEn = emitidoEn;
            Ruc = ruc;
            RazonSocial = razonSocial;
            GeneraNumeroDeSerie();
        }
        protected override void CalculaTotales()
        {
            Total = 0;
            Detalles.ForEach(x => { Total += x.Monto; });
            this.SubTotal = decimal.Round((Total / 1.10m), 2);
            this.Impuestos = Total - SubTotal;

        }

        protected override void GeneraNumeroDeSerie()
        {
            this.Serie = 1;
            this.Numero = 10;

        }

        public override string Imprimir()
        {
            StringBuilder impresion = new StringBuilder();

            impresion.AppendLine($"Recibo por Honorarios {NumeroDocumento}");
            impresion.AppendLine($"Emision: {EmitidoEn.ToString("dd-MM-yyyy")}");
            impresion.AppendLine($"Ruc {Ruc} Razón Social: {RazonSocial}");
            impresion.AppendLine("=======================Detalle================================");
            Detalles.ForEach(x =>
            {
                impresion.AppendLine($"{x.Descripcion}");
            });
            impresion.AppendLine("==============================================================");
            impresion.AppendLine($"SubTotal \t\t{SubTotal}");
            impresion.AppendLine($"Impuestos(10%)\t\t{Impuestos}");
            impresion.AppendLine($"Total\t\t\t{Total}");
            return impresion.ToString();
        }
    }
}
