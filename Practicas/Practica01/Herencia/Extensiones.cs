﻿namespace System
{
    public static class Extensiones
    {
        public static string Formateado(this int numero, int ancho)
        {
            return numero.ToString().PadLeft(ancho, '0');
        }

        public static bool NoEsNulo(this object objeto)
        {
            return (objeto != null);
        }
    }


}
