﻿using System;
using System.Collections.Generic;

namespace Herencia
{
    abstract class Comprobante
    {
        protected int Numero;
        protected int Serie;

        public Comprobante()
        {
            Detalles = new List<DetalleComprobante>();
        }
        public string NumeroDocumento => $"{Serie.Formateado(4)}-{Numero.Formateado(10)}";
        public DateTime EmitidoEn { get; protected set; }

        public string Ruc { get; protected set; }
        public string RazonSocial { get; protected set; }
        public List<DetalleComprobante> Detalles { get; protected set; }

        public decimal SubTotal { get; protected set; }
        public decimal Impuestos { get; protected set; }
        public decimal Total { get; protected set; }


        public void AgregarDetalle(DetalleComprobante detalle)
        {
            if (!detalle.NoEsNulo()) return;
            Detalles.Add(detalle);
            CalculaTotales();
        }

        public abstract string Imprimir();

        protected abstract void CalculaTotales();
        protected abstract void GeneraNumeroDeSerie();

    }
    
}
