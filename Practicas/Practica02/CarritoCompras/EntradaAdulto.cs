﻿using System;
using System.Collections.Generic;

namespace CarritoCompras
{
    internal class EntradaAdulto : Entrada
    {

        protected override void LeerPrecios()
        {
            precios = new Dictionary<DayOfWeek, decimal>();
            precios.Add(DayOfWeek.Sunday, 20);
            precios.Add(DayOfWeek.Monday, 18);
            precios.Add(DayOfWeek.Tuesday, 12);
            precios.Add(DayOfWeek.Wednesday, 18);
            precios.Add(DayOfWeek.Thursday, 18);
            precios.Add(DayOfWeek.Friday, 18);
            precios.Add(DayOfWeek.Saturday, 20);

        }
     
    }
}