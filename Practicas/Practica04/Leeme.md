# Trabajo Calificado 02
Implementar un Metodo Generica para la Actualizacion de Objetos con Relaci�n Maestro-Detalle
utilizando el Codigo de ejemplo de la clase

Ejemplo de Uso

	Album albumModificado = ObtenerAlbumModificado(1);

	Actualizar<Album,Track>(albumModificado, x=>x.AlbumId == albumModificado.AlbumId );

	Invoice invoiceModificado = ObtenerInvoiceModificado(1);

	Actualizar<Invoice,InvoiceLine>(invoiceModificado, , x=>x.InvoiceId == invoiceModificado.InvoiceId);


Esto debe Actualizar tanto el Album como el Invoice
	