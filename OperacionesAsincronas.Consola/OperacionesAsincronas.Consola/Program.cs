﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperacionesAsincronas.Consola
{
    class Program
    {
        static void Main(string[] args)
        {
            Task task = new Task(ProcesaDatosAsincrono);
            task.Start();
            task.Wait();
            Console.ReadKey();

        }


        static async void ProcesaDatosAsincrono()
        {
            Task<string> task = LeerArchivoAsincrono("C:\\uzicode\\eula.txt");

            Log("Esperemos mientras se carga el archivo");

            string x = await task;

            Log($"Conteo: {x}");
        }

        static async Task<string> LeerArchivoAsincrono(string archivo)
        {
            Log("Se entro a LeerArchivoAsincrono");
            int contador = 0;
            string texto;

            using (StreamReader reader = new StreamReader(archivo))
            {
                Log("se abrio el archivo");
                texto = await reader.ReadToEndAsync();
                Log("se obtuvo el contenido del archivo");
                contador += texto.Length;

                for(int i=0; i<10000;i++)
                {
                    int x = texto.GetHashCode();
                    if (x == 0) contador--;
                }
                Log("se termino el bucle");
            }

            Log("Se termino de leer el archivo");
            return texto;
        }

        static void Log(string mensaje)
        {
            Console.WriteLine($"{DateTime.Now.ToString("{dd/MM/yyyy HH:mm:ss.fff}")} {mensaje}");
        }
    }
}
