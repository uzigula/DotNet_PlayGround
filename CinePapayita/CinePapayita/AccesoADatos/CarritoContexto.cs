﻿using CinePapayita.Dominio;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinePapayita.AccesoADatos
{
    public class CarritoContexto : DbContext
    {
        public CarritoContexto() :base("name=CinePapayita")
        {
        }

        public IDbSet<Entrada> Entradas { get; set; }
        public IDbSet<Precio> Precios { get; set; }

        public IDbSet<Pelicula> Peliculas { get; set; }

        public IDbSet<Sala> Salas { get; set; }

        public IDbSet<Funcion> Funciones { get; set; }

        public IDbSet<Feriados> Feriados { get; set; }

        public IDbSet<Venta> Ventas { get; set; }

        public IDbSet<VentaDetalle> VentaDetalles { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Entrada>()
                .Map<MenorDeEdad>(m => m.Requires("Tipo").HasValue("ME"))
                .Map<Adulto>(m => m.Requires("Tipo").HasValue("AD"))
                .Map<AdultoMayor>(m => m.Requires("Tipo").HasValue("AM"))
                .HasMany(p => p.Precios)
                .WithRequired(e => e.Entrada)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Venta>()
                .HasMany(d => d.Tickets)
                .WithRequired(v => v.Venta)
                .WillCascadeOnDelete(true);

            base.OnModelCreating(modelBuilder);
        }
    }
}
