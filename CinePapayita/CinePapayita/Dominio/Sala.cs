﻿namespace CinePapayita.Dominio
{
    public class Sala : Entidad
    {
        public string Nombre { get; set; }
        public int Capacidad { get; set; }

    }
}