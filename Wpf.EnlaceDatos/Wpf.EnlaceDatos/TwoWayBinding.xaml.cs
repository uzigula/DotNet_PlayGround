﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Wpf.EnlaceDatos.Modelos;

namespace Wpf.EnlaceDatos
{
    /// <summary>
    /// Lógica de interacción para TwoWayBinding.xaml
    /// </summary>
    public partial class TwoWayBinding : Window
    {
        private ObservableCollection<Usuario> listaUsuarios = new ObservableCollection<Usuario>();
        public TwoWayBinding()
        {
            InitializeComponent();
            listaUsuarios.Add(new Usuario { Nombre = "Juan Perez" });
            listaUsuarios.Add(new Usuario { Nombre = "Jose Gonzales" });

            Usuarios.ItemsSource = listaUsuarios;
        }

        private void AgregarUsuario_Click(object sender, RoutedEventArgs e)
        {
            listaUsuarios.Add(new Usuario { Nombre = "Usuario Nuevo" });
        }

        private void ModificaUsuario_Click(object sender, RoutedEventArgs e)
        {
            if (Usuarios.SelectedItem != null)
                (Usuarios.SelectedItem as Usuario).Nombre = "Nombre cambiado";
        }

        private void EliminaUsuario_Click(object sender, RoutedEventArgs e)
        {
            if (Usuarios.SelectedItem != null)
                listaUsuarios.Remove(Usuarios.SelectedItem as Usuario);

        }
    }
}
