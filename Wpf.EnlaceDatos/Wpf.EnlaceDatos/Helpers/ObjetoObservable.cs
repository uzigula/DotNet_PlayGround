﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Wpf.EnlaceDatos.Helpers
{
    public class ObjetoObservable : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string nombrePropiedad)
        {
            if (this.PropertyChanged != null)
                    this.PropertyChanged(this, new PropertyChangedEventArgs(nombrePropiedad));
        }

        protected bool Set<T>(ref T valorViejo, T valorNuevo, [CallerMemberName]string nombrePropiedad = null)
        {
            return Set(nombrePropiedad, ref valorViejo, valorNuevo);
        }

        protected bool Set<T>(string nombrePropiedad, ref T valorViejo, T valorNuevo)
        {
            if (Equals(valorViejo, valorNuevo))
                return false;
            valorViejo = valorNuevo;
            NotifyPropertyChanged(nombrePropiedad);
            return true;
        }
    }
}
