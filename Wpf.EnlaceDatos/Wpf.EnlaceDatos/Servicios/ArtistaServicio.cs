﻿using System;
using System.Collections.Generic;
using System.Linq;
using Wpf.EnlaceDatos.Dominio;
using Wpf.EnlaceDatos.Modelos;

namespace Wpf.EnlaceDatos.Servicios
{
    class ArtistaServicio
    {
        private readonly Chinook db;

        public ArtistaServicio()
        {
            db = new Chinook();
        }

        internal List<ArtistaViewModel> TraerTodos()
        {
            return db.Artist.Select(x => new ArtistaViewModel
            {
                Id = x.ArtistId,
                Nombre = x.Name
            }).ToList();
        }

        internal ICollection<AlbumViewModel> TraerAlbums(int artistaId)
        {
            return db.Album
                .Where(a => a.ArtistId == artistaId)
                .Select(x => new AlbumViewModel
                {
                     Id = x.AlbumId,
                     Titulo = x.Title
                }).ToList();
        }

        internal List<CancionViewModel> TraerCanciones(int albumId)
        {
            var lista = db.Track
                .Where(t => t.AlbumId == albumId)
                .Select(x => new CancionViewModel
                {
                    Id = x.TrackId,
                    Titulo= x.Name,
                    Precio = x.UnitPrice
                }).ToList();

            return lista;
        }
    }
}