﻿using Wpf.EnlaceDatos.Helpers;

namespace Wpf.EnlaceDatos.Modelos
{
    public class CancionViewModel : ObjetoObservable
    {
        private int _id;
        private string _titulo;
        private decimal _precio;

        public int Id
        {
            get { return _id; }
            set { Set(ref _id, value); }
        }

        public string Titulo
        {
            get { return _titulo; }
            set { Set(ref _titulo, value); }
        }

        public decimal Precio
        {
            get { return _precio; }
            set { Set(ref _precio, value); }
        }
    }
}