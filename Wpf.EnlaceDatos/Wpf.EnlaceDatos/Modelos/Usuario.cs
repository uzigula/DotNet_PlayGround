﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wpf.EnlaceDatos.Modelos
{
    class Usuario : INotifyPropertyChanged
    {
        private string _nombre;
        public string Nombre
        {
            get { return _nombre; }
            set
            {
                if (this._nombre != value)
                {
                    this._nombre = value;
                    if (this.PropertyChanged!=null) this.PropertyChanged(this, new PropertyChangedEventArgs("Nombre"));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
