﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wpf.EnlaceDatos.Helpers;

namespace Wpf.EnlaceDatos.Modelos
{
    public class ArtistaViewModel : ObjetoObservable
    {
        private int _id;

        private string _nombre;
        private ObservableCollection<AlbumViewModel> _albums;
        public ObservableCollection<AlbumViewModel> Albums
        {
            get { return _albums; }
            set { Set(ref _albums, value); }
        }

        public int Id
        {
            get { return _id; }
            set { Set(ref _id, value); }
        }

        public string Nombre
        {
            get { return _nombre; }
            set { Set(ref _nombre, value); }
        }

    }
}
