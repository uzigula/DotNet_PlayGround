﻿using System.Collections.ObjectModel;
using Wpf.EnlaceDatos.Helpers;

namespace Wpf.EnlaceDatos.Modelos
{
    public class AlbumViewModel : ObjetoObservable
    {
        private ObservableCollection<CancionViewModel> _canciones;
        private int _id;
        private string _titulo;

        public ObservableCollection<CancionViewModel> Canciones {
            get { return _canciones; }
            set { Set(ref _canciones, value); }
        }

        public int Id
        {
            get { return _id; }
            set { Set(ref _id, value); }
        }

        public string Titulo
        {
            get { return _titulo; }
            set { Set(ref _titulo, value); }
        }
    }
}