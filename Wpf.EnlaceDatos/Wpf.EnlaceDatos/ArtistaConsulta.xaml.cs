﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Wpf.EnlaceDatos.Modelos;
using Wpf.EnlaceDatos.Servicios;

namespace Wpf.EnlaceDatos
{
    /// <summary>
    /// Lógica de interacción para ArtistaConsulta.xaml
    /// </summary>
    public partial class ArtistaConsulta : Window
    {
        private ArtistaServicio _artistaServicio;
        public ArtistaConsulta()
        {
            InitializeComponent();
            DataContext = this;
            _artistaServicio = new ArtistaServicio();
            Artistas = new ObservableCollection<ArtistaViewModel>(_artistaServicio.TraerTodos());
            ArtistasCombo.SelectedItem = Artistas?[0];
        }

        public ObservableCollection<ArtistaViewModel> Artistas
        {
            get { return (ObservableCollection<ArtistaViewModel>)GetValue(ArtistasProperty); }
            set { SetValue(ArtistasProperty, value); }
        }

        public static readonly DependencyProperty ArtistasProperty =
            DependencyProperty.Register("Artistas",
                typeof(ObservableCollection<ArtistaViewModel>),
                typeof(ArtistaConsulta),
                new PropertyMetadata(null)
                );

        public ArtistaViewModel ArtistaSeleccionado
        {
            get { return (ArtistaViewModel)GetValue(ArtistaSeleccionadoProperty); }
            set { SetValue(ArtistaSeleccionadoProperty, value); }
        }

        public static readonly DependencyProperty ArtistaSeleccionadoProperty =
            DependencyProperty.Register("ArtistaSeleccionado",
                typeof(ArtistaViewModel),
                typeof(ArtistaConsulta),
                new PropertyMetadata(null)
                );

        public AlbumViewModel AlbumSeleccionado
        {
            get { return (AlbumViewModel)GetValue(AlbumSeleccionadoProperty); }
            set { SetValue(AlbumSeleccionadoProperty, value); }
        }

        public static readonly DependencyProperty AlbumSeleccionadoProperty =
            DependencyProperty.Register("AlbumSeleccionado",
                typeof(AlbumViewModel),
                typeof(ArtistaConsulta),
                new PropertyMetadata(null)
                );

        private void OnArtistaSeleccionado(object sender, SelectionChangedEventArgs e)
        {
            ArtistaSeleccionado = ArtistasCombo.SelectedItem as ArtistaViewModel;

            ArtistaSeleccionado.Albums = new ObservableCollection<AlbumViewModel>(_artistaServicio.TraerAlbums(ArtistaSeleccionado.Id));

            AlbumsCombo.SelectedItem = ArtistaSeleccionado.Albums?[0];
        }

        private void OnAlbumSeleccionado(object sender, SelectionChangedEventArgs e)
        {
            if (AlbumsCombo.SelectedItem == null) return;

            AlbumSeleccionado = AlbumsCombo.SelectedItem   as AlbumViewModel;

            AlbumSeleccionado.Canciones = new ObservableCollection<CancionViewModel>(_artistaServicio.TraerCanciones(AlbumSeleccionado.Id));
        }
    }
}
